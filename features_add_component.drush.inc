<?php

class FeaturesAddComponentException extends Exception {}

/**
 * Implements hook_drush_command().
 */
function features_add_component_drush_command() {
  $items['features-add-component'] = array(
    'description' => 'Demonstrate how Drush commands work.',
    'arguments' => array(
      'feature' => 'The machine name of the feature',
      'component-type' => 'The machine name of the component type',
      'component' => 'The machine name of the component within the component type',
    ),
    'options' => array(),
    'drupal dependencies' => array('features'),
    'aliases' => array('fac'),
  );

  return $items;
}

/**
 * Command function callback.
 */
function drush_features_add_component($feature = NULL, $component_type = NULL, $component = NULL) {
  try {
    $feature or $feature = _drush_features_add_component_choose_feature();
    $component_type or $component_type = _drush_features_add_component_choose_component_type();
    $component or $component = _drush_features_add_component_choose_component($component_type);
  }
  catch (FeaturesAddComponentException $ex) {
    return drush_set_error('', $ex->getMessage());
  }

  $components = array();

  // There is always at least 1.
  $components[] = $component_type . ':' . $component;

  // If there are more, they must be factor 2.
  if (func_num_args() > 3) {
    $more = array_slice(func_get_args(), 3);
    if (count($more) % 2 != 0) {
      return drush_set_error('', 'Invalid number of arguments. If more than 3, must be factor 2 for every component_type + component.');
    }

    $more = array_chunk($more, 2);
    foreach ($more as list($component_type, $component)) {
      $components[] = $component_type . ':' . $component;
    }
  }

  drush_set_context('DRUSH_AFFIRMATIVE', 1);

  $args = array_merge(array($feature), $components);
  call_user_func_array('drush_features_export', $args);
}

/**
 * Helper, Choose a component type.
 */
function _drush_features_add_component_choose_component_type() {
  $component_types = array_keys(_drush_features_component_list());

  $choice = _drush_features_add_component_choose('Choose a component type', $component_types);
  if (!$choice) {
    throw new FeaturesAddComponentException("Invalid component type: '$choice'.");
  }

  return $choice;
}

/**
 * Helper, Choose a component.
 */
function _drush_features_add_component_choose_component($component_type) {
  $components = _drush_features_component_list();

  $component_options = array();
  $filtered_components = _drush_features_component_filter($components, array($component_type));
  foreach ($filtered_components['components'] as $source => $components) {
    foreach ($components as $name => $value) {
      $component_options[] = $name;
    }
  }

  $choice = _drush_features_add_component_choose('Choose a component', $component_options);
  if (!$choice) {
    throw new FeaturesAddComponentException("Invalid component: '$choice'.");
  }

  return $choice;
}

/**
 * Helper, Choose a feature.
 */
function _drush_features_add_component_choose_feature() {
  $features = array_keys(features_get_features());

  $choice = _drush_features_add_component_choose('Choose a feature', $features);
  if (!$choice) {
    throw new FeaturesAddComponentException("Invalid feature: '$choice'.");
  }

  return $choice;
}

/**
 * Helper to get the user's choice. Better than drush_choice() because it'll accept the keys AND labels.
 */
function _drush_features_add_component_choose($prompt, array $options) {
  natcasesort($options);

  drush_print($prompt);
  drush_print(' * ' . implode("\n * ", $options));

  $option = drush_prompt('Choose');

  if (in_array($option, $options)) {
    return $option;
  }

  throw new FeaturesAddComponentException("Invalid feature: '$choice'.");
}
